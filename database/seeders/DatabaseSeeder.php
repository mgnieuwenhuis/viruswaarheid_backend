<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(1)->create();
        \App\Models\Rubric::factory(10)->create();
        \App\Models\Tag::factory(20)->create();
        \App\Models\Video::factory(3)->create();
        \App\Models\Restream::factory(7)->create();
        \App\Models\TagVideo::factory(20)->create();
        \App\Models\Publishing::factory(30)->create();
        \App\Models\Tweeter::factory(1)->create();
        \App\Models\UserRole::factory(3)->create();
    }
}
