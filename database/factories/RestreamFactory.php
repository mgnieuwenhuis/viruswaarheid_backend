<?php

namespace Database\Factories;

use App\Models\Video;
use Illuminate\Database\Eloquent\Factories\Factory;

class RestreamFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'description' => $this->faker->text(30),
            'video_id' => Video::inRandomOrder()->first()->id,
            'scheduled_at' => $this->faker->date()
        ];
    }
}
