<?php

namespace Database\Factories;

use App\Models\Tag;
use App\Models\Video;
use Illuminate\Database\Eloquent\Factories\Factory;

class TagVideoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'video_id' => Video::inRandomOrder()->first()->id,
            'tag_id' => Tag::inRandomOrder()->first()->id,
        ];
    }
}
