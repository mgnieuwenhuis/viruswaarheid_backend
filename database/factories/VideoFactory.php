<?php

namespace Database\Factories;

use App\Models\Rubric;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class VideoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->word(),
            'publish_at' => $this->faker->date(),
            'summary' => $this->faker->text,

            'rubric_id' => Rubric::inRandomOrder()->first()->id,

            'url_media_videowaarheid' => $this->faker->url(),
            'url_article_viruswaarheid' => $this->faker->url(),

            'file_name' => 'test.txt',
            'file_size' => '12345',
            'file_type' => 'text/html',

            'thumbnail_name' => 'test.txt',
            'thumbnail_type' => 'ladskj',
            'thumbnail_size' => '12345',
        ];
    }
}
