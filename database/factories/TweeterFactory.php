<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class TweeterFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => "NDikkeboom",
            'twitter_id' => "2275398922",
        ];
    }
}
