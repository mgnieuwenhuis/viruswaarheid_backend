<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestreamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restreams', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->timestamps();
            $table->dateTime('scheduled_at')->nullable();
            $table->string('title');
            $table->text('description');

            $table->text('media')->nullable();

            $table->uuid('video_id');
            $table->index('video_id');
            $table
                ->foreign('video_id')->references('id')
                ->on('videos')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restreams');
    }
}
