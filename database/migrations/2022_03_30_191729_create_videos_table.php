<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('title');

            $table->string('file_name')->nullable();
            $table->string('file_size')->nullable();
            $table->string('file_type')->nullable();

            $table->string('thumbnail_name')->nullable();
            $table->string('thumbnail_size')->nullable();
            $table->string('thumbnail_type')->nullable();

            $table->string('attachment_name')->nullable();
            $table->string('attachment_size')->nullable();
            $table->string('attachment_type')->nullable();

            $table->longText('summary')->nullable();
            $table->string('url_media_videowaarheid')->nullable();
            $table->string('url_article_viruswaarheid')->nullable();

            $table->uuid('rubric_id')->nullable();
            $table->index('rubric_id');
            $table
                ->foreign('rubric_id')->references('id')
                ->on('rubrics')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('videos');
    }
}
