<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTweetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tweets', function (Blueprint $table) {
            $table->uuid('id')->primary();

            $table->text('text');

            $table->bigInteger('twitter_id');
            $table->dateTime('tweet_created_at');

            $table->uuid('tweeter_id');
            $table->index('tweeter_id');
            $table
                ->foreign('tweeter_id')->references('id')
                ->on('tweeters')->onDelete('cascade')->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tweets');
    }
}
