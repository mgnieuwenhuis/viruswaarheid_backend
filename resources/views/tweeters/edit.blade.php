<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tweeter bewerken') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('tweeters.update', [$tweeter->id]) }}">
                        @csrf
                        @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                        @endif
                        <div>
                            <x-label for="username" :value="__('Username')" />

                            <x-input id="username" class="block mt-1 w-full" type="text" name="username" :value="old('username', $tweeter->username)" required autofocus />
                        </div>

                        <div>
                            <x-label for="twitter_id" :value="__('Twitter ID')" />

                            <x-input id="twitter_id" class="block mt-1 w-full" type="text" name="twitter_id" :value="old('twitter_id', $tweeter->twitter_id)" required autofocus />
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-3">
                                {{ __('Sla tweeter op') }}
                            </x-button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
