<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Alle tweeters') }}
        </h2>
    </x-slot>

        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="GET" action="{{ route('tweeters.index') }}">
                        @csrf
                        <div>
                            <x-label for="username" :value="__('Gebruikersnaam')" />

                            <x-input id="username" class="block mt-1 w-full" type="text" name="filter[username]" :value="old('username')" autofocus />
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-3">
                                {{ __('Zoek') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    {{ $tweeters->links() }}
                    <table>
                        <thead class="bg-gray-50">
                        <tr>
                            <th class="px-6 py-2 text-lg text-gray-500">
                                {{ __('Gebruikersnaam') }}
                            </th>
                            <th class="px-6 py-2 text-lg text-gray-500">
                                {{ __('Twitter ID') }}
                            </th>
                            <th class="px-6 py-2 text-lg text-gray-500 ">
                                {{ __('Bewerken') }}
                            </th>
                            <th class="px-6 py-2 text-lg text-gray-500 ">
                                {{ __('Verwijderen') }}
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white">
                        @foreach($tweeters as $tweeter)
                        <tr>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('tweeters.show', [$tweeter->id]) }}" class="px-4 py-1 text-sm">{{ $tweeter->username }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                {{ $tweeter->twitter_id }}
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('tweeters.edit', [$tweeter->id]) }}" class="px-4 py-1 text-sm">{{ __('Bewerken') }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a style="color: red" href="{{ route('tweeters.destroy', [$tweeter->id]) }}" class="px-4 py-1 text-sm">{{ __('Verwijderen') }}</a>
                            </td>
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $tweeters->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
