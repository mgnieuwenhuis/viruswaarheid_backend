<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tweeter: ' .  $tweeter->username) }}
        </h2>
        <a href="{{ route('tweeters.edit', ['tweeter' => $tweeter->id]) }}">Bewerken</a>
         |
        <a href="{{ route('tweeters.destroy', ['tweeter' => $tweeter->id]) }}" style="color: red">Verwijderen</a>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Username') }}</h1>
                    <p>{{ $tweeter->username }}</p>
                </div>
            </div>

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Twitter ID') }}</h1>
                    <p>{{ $tweeter->twitter_id }}</p>
                </div>
            </div>

        </div>
    </div>
</x-app-layout>
