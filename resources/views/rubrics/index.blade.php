<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Alle rubrieken') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="GET" action="{{ route('rubrics.index') }}">
                        @csrf
                        <div>
                            <x-label for="title" :value="__('Titel')"/>

                            <x-input id="title" class="block mt-1 w-full" type="text" name="filter[title]"
                                     :value="old('title')" autofocus/>
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-3">
                                {{ __('Zoek') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                {{ $rubrics->links() }}
                <table>
                    <thead class="bg-gray-50">
                    <tr>
                        <th class="px-6 py-2 text-lg text-gray-500">
                            {{ __('Titel') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500">
                            {{ __('Videos') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500 ">
                            {{ __('Bewerken') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500 ">
                            {{ __('Verwijderen') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white">
                    @foreach($rubrics as $rubric)
                        <tr>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('rubrics.show', [$rubric->id]) }}"
                                   class="px-4 py-1 text-sm">{{ $rubric->title }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                {{ $rubric->videos->count() }}x
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('rubrics.edit', [$rubric->id]) }}"
                                   class="px-4 py-1 text-sm">{{ __('Bewerken') }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a style="color: red" href="{{ route('rubrics.destroy', [$rubric->id]) }}"
                                   class="px-4 py-1 text-sm">{{ __('Verwijderen') }}</a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                {{ $rubrics->links() }}

            </div>
        </div>
    </div>
</x-app-layout>
