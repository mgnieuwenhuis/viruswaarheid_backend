<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Rubriek bewerken') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('rubrics.update', [$rubric->id]) }}">
                        @csrf
                        @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                        @endif
                        <div>
                            <x-label for="title" :value="__('Titel')" />

                            <x-input id="title" class="block mt-1 w-full" type="text" name="title" :value="old('title', $tag->title)" required autofocus />
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-3">
                                {{ __('Sla rubriek op') }}
                            </x-button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
