<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Rubric: ' .  $rubric->title) }}
        </h2>
        <a href="{{ route('rubrics.edit', ['rubric' => $rubric->id]) }}">Bewerken</a>
         |
        <a href="{{ route('rubrics.destroy', ['rubric' => $rubric->id]) }}" style="color: red">Verwijderen</a>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Titel') }}</h1>
                    <p>{{ $rubric->title }}</p>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
