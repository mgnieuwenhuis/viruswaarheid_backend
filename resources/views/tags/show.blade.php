<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tag: ' .  $tag->title) }}
        </h2>
        <a href="{{ route('tags.edit', ['tag' => $tag->id]) }}">Bewerken</a>
         |
        <a href="{{ route('tags.destroy', ['tag' => $tag->id]) }}" style="color: red">Verwijderen</a>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Titel') }}</h1>
                    <p>{{ $tag->title }}</p>
                </div>
            </div>

            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Beschrijving') }}</h1>
                    <p>{{ $tag->description }}</p>
                </div>
            </div>

            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Videos') }}</h1>
                    <ul>
                        @foreach($tag->videos as $video)
                            <li><a href="{{ route('videos.show', ['video' => $video->id]) }}">{{ $video->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
