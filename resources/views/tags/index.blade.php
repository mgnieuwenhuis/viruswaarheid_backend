<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Alle tags') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="GET" action="{{ route('tags.index') }}">
                        @csrf
                        <div>
                            <x-label for="title" :value="__('Titel')"/>

                            <x-input id="title" class="block mt-1 w-full" type="text" name="filter[title]"
                                     :value="old('title')" autofocus/>
                        </div>
                        <div>
                            <x-label for="description" :value="__('Beschrijving')"/>

                            <x-input id="description" class="block mt-1 w-full" type="text" name="filter[description]"
                                     :value="old('description')" autofocus/>
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-3">
                                {{ __('Zoek') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                {{ $tags->links() }}
                <table>
                    <thead class="bg-gray-50">
                    <tr>
                        <th class="px-6 py-2 text-lg text-gray-500">
                            {{ __('Titel') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500">
                            {{ __('Beschrijving') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500">
                            {{ __('Videos') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500 ">
                            {{ __('Bewerken') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500 ">
                            {{ __('Verwijderen') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white">
                    @foreach($tags as $tag)
                        <tr>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('tags.show', [$tag->id]) }}"
                                   class="px-4 py-1 text-sm">{{ $tag->title }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                {{ Str::of($tag->description)->limit(20) }}
                            </td>
                            <td class="px-6 py-4 text-sm">
                                {{ $tag->videos->count() }}x
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('tags.edit', [$tag->id]) }}"
                                   class="px-4 py-1 text-sm">{{ __('Bewerken') }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a style="color: red" href="{{ route('tags.destroy', [$tag->id]) }}"
                                   class="px-4 py-1 text-sm">{{ __('Verwijderen') }}</a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                {{ $tags->links() }}

            </div>
        </div>
    </div>
</x-app-layout>
