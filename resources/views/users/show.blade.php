<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('User: ' .  $user->name) }}
        </h2>
        <a href="{{ route('users.edit', ['user' => $user->id]) }}">Bewerken</a>
         |
        <a href="{{ route('users.destroy', ['user' => $user->id]) }}" style="color: red">Verwijderen</a>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Naam') }}</h1>
                    <p>{{ $user->name }}</p>
                </div>
            </div>

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Email') }}</h1>
                    <p>{{ $user->email }}</p>
                </div>
            </div>

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Is Admin') }}</h1>
                    <p>{{ $user->isAdmin() ? 'X' : '' }}</p>
                </div>
            </div>

            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Is Redacteur') }}</h1>
                    <p>{{ $user->IsEditor() ? 'X' : '' }}</p>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
