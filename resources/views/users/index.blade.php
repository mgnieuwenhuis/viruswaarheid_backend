<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Alle gebruikers') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="GET" action="{{ route('users.index') }}">
                        @csrf
                        <div>
                            <x-label for="name" :value="__('Naam')" />

                            <x-input id="name" class="block mt-1 w-full" type="text" name="filter[title]" :value="old('name')" autofocus />
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-3">
                                {{ __('Zoek') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    {{ $users->links() }}
                    <table>
                        <thead class="bg-gray-50">
                        <tr>
                            <th class="px-6 py-2 text-lg text-gray-500">
                                {{ __('Naam') }}
                            </th>
                            <th class="px-6 py-2 text-lg text-gray-500">
                                {{ __('Email') }}
                            </th>
                            @if(Auth::user()->isAdmin())
                            <th class="px-6 py-2 text-lg text-gray-500 ">
                                {{ __('Bewerken') }}
                            </th>
                            <th class="px-6 py-2 text-lg text-gray-500 ">
                                {{ __('Verwijderen') }}
                            </th>
                            @endif
                        </tr>
                        </thead>
                        <tbody class="bg-white">
                        @foreach($users as $user)
                        <tr>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('users.show', [$user->id]) }}" class="px-4 py-1 text-sm">{{ $user->name }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                {{ $user->email }}
                            </td>
                            @if(Auth::user()->isAdmin())
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('users.edit', [$user->id]) }}" class="px-4 py-1 text-sm">{{ __('Bewerken') }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a style="color: red" href="{{ route('users.destroy', [$user->id]) }}" class="px-4 py-1 text-sm">{{ __('Verwijderen') }}</a>
                            </td>
                            @endif
                        </tr>
                        @endforeach

                        </tbody>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
