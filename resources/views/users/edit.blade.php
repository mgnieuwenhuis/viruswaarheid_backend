<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Gebruiker bewerken') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('users.update', [$user->id]) }}">
                        @csrf
                        @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                        @endif
                        <div>
                            <x-label for="name" :value="__('Naam')" />

                            <x-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name', $user->name)" required autofocus />
                        </div>

                        <div>
                            <x-label for="email" :value="__('E-mail')" />

                            <x-input id="email" class="block mt-1 w-full" type="text" name="email" :value="old('email', $user->email)" required />
                        </div>

                        <div>
                            <x-label for="password" :value="__('Wachtwoord')" />

                            <x-input id="password" class="block mt-1 w-full" type="password" name="password" :value="old('password')" required />
                        </div>

                        <div>
                            <x-label for="admin" :value="__('Is Admin')" />

                            <input id="admin" class="block mt-1" type="checkbox" name="admin" value="old('admin', 1)" {{ $user->isAdmin() ? 'checked="checked"' : '' }} />
                        </div>

                        <div>
                            <x-label for="editor" :value="__('Is Editor')" />

                            <input id="editor" class="block mt-1" type="checkbox" name="editor" value="old('editor', 1)" {{ $user->isEditor() ? 'checked="checked"' : '' }}/>
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-3">
                                {{ __('Sla gebruiker op') }}
                            </x-button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>
