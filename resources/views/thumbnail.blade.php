<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <style>
            div.imageSub img { z-index: 1; margin: 0; display: block; }
            div.imageSub div {
                position: relative;
                margin: -{{ ($height / 8) + 5 }}px 0 0;
                padding: 5px;
                height: {{ ($height / 8) - 5 }}px;
                line-height: {{ (($height / 8) - 5 ) }}px;
                text-align: center;
                overflow: hidden;
            }
            div.imageSub div.blackbg {
                z-index: 2;
                background-color: #000;
                -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=70)";
                filter: alpha(opacity=70);
                opacity: 0.5;
            }
            div.imageSub div.label {
                z-index: 3;
                color: white;
                font-size: {{ ($height / 8) }}px;
            }
        </style>
        <title>Thumbnail</title>
    </head>
    <body>
        <div class="imageSub" style="width: {{ $width }}px">
            <img src="/images/{{$video->thumbnail_name}}" alt="Something"/>
            <div class="blackbg"></div>
            <div class="label">{{$video->title}}</div>
        </div>
    </body>
</html>
