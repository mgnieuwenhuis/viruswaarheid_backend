<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Upload video') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form id="uploadForm" enctype="multipart/form-data">
                        @csrf
                        <label>Kies Bestand:</label>
                        <input type="file" name="video" id="fileInput">
                        <input type="submit" name="submit" value="UPLOAD"/>
                    </form>


                    <div class="progress">
                        <div class="progress-bar" style="font-size: 25pt"></div>
                    </div>
                    <div id="uploadStatus"></div>
                    <div id="continue" style="display: none;">
                        <x-button class="ml-3">
                            {{ __('Naar video') }}
                        </x-button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        window.addEventListener('load', function() {
            // File upload via Ajax
            $("#uploadForm").on('submit', function(e){
                e.preventDefault();
                $.ajax({
                    xhr: function() {
                        var xhr = new window.XMLHttpRequest();
                        xhr.upload.addEventListener("progress", function(evt) {
                            if (evt.lengthComputable) {
                                var percentComplete = parseInt((evt.loaded / evt.total) * 100);
                                $(".progress-bar").width(percentComplete + '%');
                                $(".progress-bar").html(percentComplete+'%');
                            }
                        }, false);
                        return xhr;
                    },
                    type: 'POST',
                    url: '{{ route('videos.do-upload-video', ['video' => $video->id]) }}',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $(".progress-bar").width('0%');
                        $('#uploadStatus').html('<img src="/images/loading.gif" style="width:150px;"/>');
                    },
                    error:function(){
                        $('#uploadStatus').html('<p style="color:#EA4335;">Video uploaden gefaald, contact support.</p>');
                    },
                    success: function(resp){
                        if(resp.success === true){
                            $('#uploadForm').hide();
                            $('#continue').show();

                            $('button[type=submit]').click(function () {
                                window.location = '{{ route('videos.show', ['video' => $video->id]) }}';
                            })

                            $('#uploadStatus').html('<p style="color:#28A74B;">Video is successvol geupload!</p>');
                        } else if (resp.error) {
                            $(".progress-bar").width('0%');
                            $('#uploadForm')[0].reset()
                            $('#uploadStatus').html('<p style="color:#EA4335;">' + resp.error.video[0] + '</p>');
                        }
                    }
                });
            });

            // File type validation
            $("#fileInput").change(function(){
                var allowedTypes = [
                    'video/x-ms-asf','video/x-flv','video/mp4','application/x-mpegURL','video/MP2T','video/3gpp','video/quicktime','video/x-msvideo','video/x-ms-wmv','video/avi'
                ];
                var file = this.files[0];
                var fileType = file.type;
                if(!allowedTypes.includes(fileType)){
                    alert('Selecteer een goed video bestand.');
                    $("#fileInput").val('');
                    return false;
                }
            });
        });
    </script>
</x-app-layout>
