<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Video: ' .  $video->title) }}
        </h2>
        <a href="{{ route('videos.edit', ['video' => $video->id]) }}">Bewerken</a>
         |
        <a href="{{ route('videos.destroy', ['video' => $video->id]) }}" style="color: red">Verwijderen</a>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Titel') }}</h1>
                    <p>{{ $video->title }}</p>
                </div>
            </div>
            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Samenvatting') }}</h1>
                    <p>{!! $video->summary !!}</p>
                </div>
            </div>
            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Rubriek') }}</h1>
                    <p>{{ $video->rubric->title }}</p>
                </div>
            </div>
            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Tags') }}</h1>
                    <p>{{ $video->tags->pluck('title')->implode(', ') }}</p>
                </div>
            </div>
            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Video') }}</h1>
                    <p><a href="{{ route('videos.download-video', ['video' => $video->id]) }}" target="_blank">{{ __('Download video') }}</a> </p>
                </div>
            </div>
            <div class="mt-4 bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <h1>{{ __('Bijlage') }}</h1>
                    @if($video->attachment_name === null)
                        <p>Geen bijlage beschikbaar</p>
                    @else
                        <p><a href="{{ route('videos.download-attachment', ['video' => $video->id]) }}" target="_blank">{{ __('Download bijlage') }}</a> </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
