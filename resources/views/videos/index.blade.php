<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Alle videos') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="GET" action="{{ route('videos.index') }}" enctype="multipart/form-data">
                        @csrf
                        <div>
                            <x-label for="title" :value="__('Titel')"/>

                            <x-input id="title" class="block mt-1 w-full" type="text" name="filter[title]"
                                     :value="old('title')" autofocus/>
                        </div>
                        <div>
                            <x-label for="summary" :value="__('Samenvatting')"/>

                            <x-input id="summary" class="block mt-1 w-full" type="text" name="filter[summary]"
                                     :value="old('title')" autofocus/>
                        </div>
                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-3">
                                {{ __('Zoek') }}
                            </x-button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
            <div class="p-6 bg-white border-b border-gray-200">
                {{ $videos->links() }}
                <table>
                    <thead class="bg-gray-50">
                    <tr>
                        <th class="px-6 py-2 text-lg text-gray-500">
                            {{ __('Titel') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500">
                            {{ __('Rubriek') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500">
                            {{ __('Tags') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500 ">
                            {{ __('Video') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500 ">
                            {{ __('Bijlage') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500 ">
                            {{ __('Publishing') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500 ">
                            {{ __('Bewerken') }}
                        </th>
                        <th class="px-6 py-2 text-lg text-gray-500 ">
                            {{ __('Verwijderen') }}
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white">
                    @foreach($videos as $video)
                        <tr>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('videos.show', [$video->id]) }}"
                                   class="px-4 py-1 text-sm">{{ $video->title }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                {{ $video->rubric->title }}
                            </td>
                            <td class="px-6 py-4 text-sm">
                                {{ $video->tags->pluck('title')->implode(', ') }}
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('videos.download-video', [$video->id]) }}"
                                   class="px-4 py-1 text-sm">{{ __('Download') }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                @if($video->attachment_name !== null)
                                    <a href="{{ route('videos.download-attachment', [$video->id]) }}"
                                       class="px-4 py-1 text-sm">{{ __('Download') }}</a>
                                @endif
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('videos.publishing.create', [$video->id]) }}"
                                       class="px-4 py-1 text-sm">{{ __('Maak publishing') }}</a>

                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a href="{{ route('videos.edit', [$video->id]) }}"
                                   class="px-4 py-1 text-sm">{{ __('Bewerken') }}</a>
                            </td>
                            <td class="px-6 py-4 text-sm">
                                <a style="color: red" href="{{ route('videos.destroy', [$video->id]) }}"
                                   class="px-4 py-1 text-sm">{{ __('Verwijderen') }}</a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
                {{ $videos->links() }}

            </div>
        </div>
    </div>

</x-app-layout>
