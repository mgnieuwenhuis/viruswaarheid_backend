<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Video bewerken') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="POST" action="{{ route('videos.update', [$video->id]) }}" enctype="multipart/form-data">
                        @csrf
                        @if($errors->any())
                            <h4>{{$errors->first()}}</h4>
                        @endif
                        <div>
                            <x-label for="title" :value="__('Titel')" />

                            <x-input id="email" class="block mt-1 w-full" type="text" name="title" :value="old('title', $video->title)" required autofocus />
                        </div>

                        <div>
                            <x-label for="summary" :value="__('Samenvatting')" />
                            <textarea id="summary" name="summary" style="width: 100%; height: 400px">{{ old('summary', $video->summary) }}</textarea>
                        </div>

                        <div>
                            <x-label for="rubric_id" :value="__('Rubriek')" />
                            <select name="rubric_id" id="rubric_id" multiple class="block mt-1 w-full h-100">
                                <option value="">Selecteer...</option>
                                @foreach(\App\Models\Rubric::all() as $rubric)
                                    <option value="{{ $rubric->id }}" {{ (old('rubric_id', $video->rubric->id) === $rubric->id ? 'selected' : '') }}>{{ $rubric->title }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div>
                            <x-label for="tags" :value="__('Tags')" />
                            <input type="text" name="tags" id="tags" value="{{ $video->tags->pluck('id')->implode(',') }}" />
                        </div>

                        <div>
                            <x-label for="attachment" :value="__('Bijlage')" />
                            <input type="file" name="attachment">
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <x-button class="ml-3">
                                {{ __('Sla video') }}
                            </x-button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <script>
        window.addEventListener('load', function() {
            $('#tags').change(function () {
                $('#tags-summary').text($('#tags option:selected').toArray().map(item => item.text).join());
            });

            const editor = SUNEDITOR.create('summary',{
                buttonList: [
                    ['undo', 'redo'],
                    ['font', 'fontSize', 'formatBlock'],
                    ['paragraphStyle', 'blockquote'],
                    ['bold', 'underline', 'italic', 'strike', 'subscript', 'superscript'],
                    ['fontColor', 'hiliteColor', 'textStyle'],
                    ['removeFormat'],
                    '/', // Line break
                    ['outdent', 'indent'],
                    ['align', 'horizontalRule', 'list', 'lineHeight'],
                    ['table', 'link'], // You must add the 'katex' library at options to use the 'math' plugin.
                    /** ['imageGallery'] */ // You must add the "imageGalleryUrl".
                    ['fullScreen', 'showBlocks', 'codeView'],
                    ['preview', 'print'],
                ]
            });

            editor.onChange = (contents, core) => {
                $('#summary').val(contents);
            }

            var $select = $('#tags').selectize({
                valueField: 'id',
                labelField: 'value',
                searchField: 'value',
                options: [
                        @foreach ($video->tags as $tag)
                    {id: '{{ $tag->id }}', value: '{{ $tag->title . ' ' . $tag->value }}'},
                    @endforeach
                ],
                create:true,
                render: {
                    option: function(item, escape) {

                        return '<div>' +
                            '<span class="name">' + escape(item.value) + '</span>' +
                            '</div>';
                    }

                },
                load: function(query, callback) {
                    if (!query.length) return callback();
                    $.ajax({
                        url: '/tags/autocomplete',
                        type: 'GET',
                        data: {
                            term: query
                        },
                        error: function() {
                            console.log('error');
                            callback();
                        },
                        success: function(res) {
                            //console.log(res[0].value);
                            callback(res);
                        }
                    });
                }

            });
        });
    </script>

</x-app-layout>
