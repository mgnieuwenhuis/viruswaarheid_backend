<?php

use App\Http\Controllers\VideoPublishController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\VideoController;
use \App\Http\Controllers\TweetController;
use App\Http\Controllers\RubricController;
use \App\Http\Controllers\TweeterController;
use App\Http\Controllers\RestreamController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard');
})->name('home');


Route::group(['middleware' => ['auth', 'admin']], function() {
    Route::post('users/create', [UserController::class, 'create']);
    Route::get('users/edit/{user}',  [UserController::class, 'edit'])->name('users.edit');
    Route::post('users/update/{user}', [UserController::class, 'update'])->name('users.update');
    Route::get('users/delete/{user}', [UserController::class, 'destroy'])->name('users.destroy');
});

Route::group(['middleware' => ['auth']], function() {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');

    Route::get('users', [UserController::class, 'list'])->name('users.index');;
    Route::get('users/{user}', [UserController::class, 'show'])->name('users.show');

    Route::get('videos', [VideoController::class, 'list'])->name('videos.index');
    Route::get('videos/unpublished', [VideoController::class, 'unpublished'])->name('videos.unpublished');
    Route::post('videos/create', [VideoController::class, 'store'])->name('videos.create');
    Route::get('videos/{video}', [VideoController::class, 'show'])->name('videos.show');
    Route::get('videos/edit/{video}',  [VideoController::class, 'edit'])->name('videos.edit');
    Route::post('videos/update/{video}',  [VideoController::class, 'update'])->name('videos.update');
    Route::get('videos/download-video/{video}',  [VideoController::class, 'downloadVideo'])->name('videos.download-video');
    Route::get('videos/download-attachment/{video}',  [VideoController::class, 'downloadAttachment'])->name('videos.download-attachment');
    Route::get('videos/upload-video/{video}',  [VideoController::class, 'uploadVideo'])->name('videos.upload-video');
    Route::post('videos/upload-video/{video}',  [VideoController::class, 'doUploadVideo'])->name('videos.do-upload-video');
    Route::get('videos/delete/{video}',  [VideoController::class, 'destroy'])->name('videos.destroy');
    Route::get('videos/{video}/ /create',  [VideoPublishController::class, 'create'])->name('videos.publishing.create');

    Route::get('rubrics', [RubricController::class, 'list'])->name('rubrics.index');;
    Route::post('rubrics/create', [RubricController::class, 'store']);
    Route::get('rubrics/{rubric}', [RubricController::class, 'show'])->name('rubrics.show');
    Route::get('rubrics/edit/{rubric}',  [RubricController::class, 'edit'])->name('rubrics.edit');
    Route::post('rubrics/update/{rubric}',  [RubricController::class, 'update'])->name('rubrics.update');
    Route::get('rubrics/delete/{rubric}',  [RubricController::class, 'destroy'])->name('rubrics.destroy');

    Route::get('restreams', [RestreamController::class, 'list']);
    Route::post('restreams/create', [RestreamController::class, 'store']);
    Route::get('restreams/{restream}', [RestreamController::class, 'show']);
    Route::put('restreams/update/{restream}',  [RestreamController::class, 'update']);
    Route::delete('restreams/delete/{restream}',  [RestreamController::class, 'destroy']);

    Route::get('tags', [TagController::class, 'list'])->name('tags.index');;
    Route::get('tags/autocomplete', [TagController::class, 'autocomplete']);
    Route::post('tags/create', [TagController::class, 'store']);
    Route::get('tags/{tag}', [TagController::class, 'show'])->name('tags.show');
    Route::get('tags/edit/{tag}',  [TagController::class, 'edit'])->name('tags.edit');
    Route::post('tags/update/{tag}',  [TagController::class, 'update'])->name('tags.update');
    Route::get('tags/delete/{tag}',  [TagController::class, 'destroy'])->name('tags.destroy');

    Route::get('tweeters', [TweeterController::class, 'list'])->name('tweeters.index');;
    Route::post('tweeters/create', [TweeterController::class, 'store']);
    Route::get('tweeters/{tweeter}', [TweeterController::class, 'show'])->name('tweeters.show');
    Route::get('tweeters/edit/{tweeter}',  [TweeterController::class, 'edit'])->name('tweeters.edit');
    Route::post('tweeters/update/{tweeter}',  [TweeterController::class, 'update'])->name('tweeters.update');
    Route::get('tweeters/delete/{tweeter}',  [TweeterController::class, 'destroy'])->name('tweeters.destroy');

    Route::get('tweeters/{tweeter}/tweets', [TweetController::class, 'list'])->name('tweets.index');;
    Route::get('tweeters/{tweeter}/tweets/{tweed}', [TweetController::class, 'show']);
});

require __DIR__.'/auth.php';
