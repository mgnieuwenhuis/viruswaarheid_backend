<?php

return [
    'app_id' => env('FACEBOOK_APP_ID'),
    'app_secret' => env('FACEBOOK_APP_SECRET'),
    'page_token' => env('FACEBOOK_PAGE_TOKEN'),
];
