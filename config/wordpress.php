<?php

return [
    'rpc_url' => env('WORDPRESS_RPC_URL'),
    'username' => env('WORDPRESS_USERNAME'),
    'password' => env('WORDPRESS_PASSWORD'),
    'category' => env('WORDPRESS_CATEGORY'),
];
