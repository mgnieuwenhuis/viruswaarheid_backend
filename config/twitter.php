<?php


return [
    'app_key' => env('TWITTER_APP_KEY'),
    'app_secret' => env('TWITTER_APP_SECRET'),
    'access_token' => env('TWITTER_ACCESS_TOKEN'),
    'access_token_secret' => env('TWITTER_ACCESS_TOKEN_SECRET'),
];
