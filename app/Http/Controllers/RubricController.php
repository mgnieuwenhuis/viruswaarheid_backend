<?php
namespace App\Http\Controllers;

use App\Models\Rubric;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class RubricController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function list(Request $request)
    {
        $validator = Validator::make($request->only('filter'), [
            'sort' => 'array',
            'filter' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $rubrics = Rubric::filter($request->filter)->sort($request->sort)->paginate(25);

        return view('rubrics.index', compact('rubrics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        //Validate data
        $data = $request->only(
            'title')
        ;
        $validator = Validator::make($data, [
            'title' => 'required|string'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is valid, create new rubric
        $rubric = Rubric::create([
            'title' => $request->title
        ]);

        //Video created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Rubric created successfully',
            'data' => $rubric
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Rubric $rubric
     * @return JsonResponse
     */
    public function show(Rubric $rubric)
    {
        if (!$rubric) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, rubric not found.'
            ], 400);
        }

        return $rubric;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Rubric $rubric
     */
    public function update(Request $request, Rubric $rubric)
    {
        //Validate data
        $data = $request->only('title');
        $validator = Validator::make($data, [
            'title' => 'required|string'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['message' => $validator->messages()->first()]);
        }

        //Request is valid, update rubric
        $rubric->update([
            'title' => $request->title
        ]);

        return redirect()->route('rubrics.show', ['rubric' => $rubric->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Rubric $rubric
     */
    public function destroy(Rubric $rubric)
    {
        $rubric->delete();

        return redirect()->route('rubrics.index');
    }

    public function edit(Request $request, Rubric $rubric)
    {
        return view('rubrics.edit', compact('rubric'));
    }
}
