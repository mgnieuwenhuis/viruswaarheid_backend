<?php

namespace App\Http\Controllers;

use App\Models\Tweeter;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class TweeterController extends Controller
{
    /**
     * List all monitored tweeters
     * @param Request $request
     */
    public function list(Request $request)
    {
        $validator = Validator::make($request->only('filter'), [
            'sort' => 'array',
            'filter' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $tweeters = Tweeter::filter($request->filter)->sort($request->sort)->paginate(25);

        return view('tweeters.index', compact('tweeters'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        //Validate data
        $data = $request->only(
            'username', 'twitter_id');

        $validator = Validator::make($data, [
            'username' => 'required|string',
            'twitter_id' => 'required|numeric'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }
        //Request is valid, create new tweeter
        $tweeter = Tweeter::create([
            'username' => $request->username,
            'twitter_id' => $request->twitter_id,
        ]);

        //Tweeter created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Tweeter created successfully',
            'data' => $tweeter
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Tweeter $tweeter
     * @return Tweeter
     */
    public function show(Tweeter $tweeter)
    {
        if (!$tweeter) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, tweeter not found.'
            ], 400);
        }

        return $tweeter;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Tweeter $tweeter
     */
    public function update(Request $request, Tweeter $tweeter)
    {
        //Validate data
        $data = $request->only(
            'username', 'twitter_id');

        $validator = Validator::make($data, [
            'username' => 'required|string',
            'twitter_id' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['message' => $validator->messages()->first()]);
        }

        //Request is valid, update tweeter
        $tweeter->update([
            'username' => $request->username,
            'twitter_id' => $request->twitter_id,
        ]);

        return redirect()->route('tweeters.show', ['tweeter' => $tweeter->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tweeter $tweeter
     */
    public function destroy(Tweeter $tweeter)
    {
        $tweeter->delete();

        return redirect()->route('tweeters.index');
    }

    public function edit(Request $request, Tweeter $tweeter)
    {
        return view('tweeters.edit', compact('tweeter'));
    }
}
