<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserRole;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function create(Request $request)
    {
        //Validate data
        $data = $request->only('name', 'email', 'password', 'admin');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'admin' => 'required|boolean',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|max:50',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is valid, create new user
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'admin' => $request->admin,
            'password' => bcrypt($request->password),
        ]);

        //User created, return success response
        return response()->json([
            'success' => true,
            'message' => 'User created successfully',
            'data' => $user
        ], Response::HTTP_OK);
    }

    public function destroy(Request $request, User $user)
    {
        $user->delete();

        return redirect()->route('users.index');
    }

    public function list(Request $request)
    {
        $validator = Validator::make($request->only('filter'), [
            'sort' => 'array',
            'filter' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $users = User::filter($request->filter)->sort($request->sort)->paginate(25);

        return view('users.index', compact("users"));
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     */
    public function show(User $user)
    {
        if (!$user) {
            return redirect()->route('dashboard');
        }

        return view('users.show', compact("user"));
    }

    public function update(Request $request, User $user)
    {
        //Validate data
        $data = $request->only('name', 'email', 'password', 'admin', 'editor');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'email' => ['required','email', Rule::unique('users')->ignore($user->id)],
            'password' => 'required|string|min:6|max:50',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['message' => $validator->messages()->first()]);
        }

        //Request is valid, update user
        $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        if($request->admin) {
            $user->addRole(UserRole::ADMIN);
        } else {
            $user->removeRole(UserRole::ADMIN);
        }

        if($request->editor) {
            $user->addRole(UserRole::EDITOR);
        } else {
            $user->removeRole(UserRole::EDITOR);
        }

        return redirect()->route('users.show', ['user' => $user->id]);
    }

    public function edit(Request $request, User $user)
    {
        return view('users.edit', compact('user'));
    }
}
