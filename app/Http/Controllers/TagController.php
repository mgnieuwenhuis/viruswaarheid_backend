<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class TagController extends Controller
{
    public function autocomplete(Request $request)
    {
        $validator = Validator::make($request->only('term'), [
            'term' => 'string',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        return Tag::filter(['title' => $request->term])->get(['id', 'title as value']);
    }

    public function list(Request $request)
    {
        $validator = Validator::make($request->only('filter'), [
            'sort' => 'array',
            'filter' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $tags = Tag::filter($request->filter)->sort($request->sort)->paginate(25);

        return view('tags.index', compact("tags"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        //Validate data
        $data = $request->only(
            'title', 'description');
        $validator = Validator::make($data, [
            'title' => 'required|string',
            'description' => 'string',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is valid, create new tag
        $tag = Tag::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        $tag->videos()->sync($request->videos);

        //Video created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Tag created successfully',
            'data' => $tag->loadMissing('videos')
        ], Response::HTTP_OK);
    }

    public function show(Tag $tag)
    {
        if (!$tag) {
            return response()->redirectToAction('home');
        }

        $tag->loadMissing('videos');

        return view('tags.show', compact("tag"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Tag $tag
     */
    public function update(Request $request, Tag $tag)
    {
        //Validate data
        $data = $request->only('title', 'description');
        $validator = Validator::make($data, [
            'title' => 'required|string',
            'description' => 'string',
            'videos' => 'array'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['message' => $validator->messages()->first()]);
        }

        //Request is valid, update tag
        $tag->update([
            'title' => $request->title,
            'description' => $request->description
        ]);

        $tag->videos()->sync($request->videos);

        return redirect()->route('tags.show', ['tag' => $tag->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag
     */
    public function destroy(Tag $tag)
    {
        $tag->delete();

        return redirect()->route('tags.index');
    }

    public function edit(Request $request, Tag $tag)
    {
        return view('tags.edit', compact('tag'));
    }
}
