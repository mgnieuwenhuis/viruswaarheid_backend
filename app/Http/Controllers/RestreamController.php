<?php

namespace App\Http\Controllers;

use App\Models\Restream;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class RestreamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Pagination\Paginator
     */
    public function list(Request$request)
    {
        $validator = Validator::make($request->only('filter'), [
            'sort' => 'array',
            'filter' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        return Restream::filter($request->filter)->sort($request->sort)->paginate(25);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        //Validate data
        $data = $request->only(
            'video_id','scheduled_at','title', 'description');

        $validator = Validator::make($data, [
            'title'         => 'required|string',
            'video_id'      => 'required|exists:videos,id',
            'scheduled_at'  => 'date',
            'description'   => 'required|string'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is valid, create new restream
        $restream = Restream::create([
            'title'         => $request->title,
            'video_id'      => $request->video_id,
            'description'   => $request->description,
            'scheduled_at'  => $request->scheduled_at,
        ]);

        //Video created, return success response
        return response()->json([
            'success' => true,
            'message' => 'Restream created successfully',
            'data' => $restream
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param Restream $restream
     * @return JsonResponse
     */
    public function show(Restream $restream)
    {
        if (!$restream) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, restream not found.'
            ], 400);
        }

        return $restream;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Restream $restream
     * @return JsonResponse
     */
    public function update(Request $request, Restream $restream)
    {
        //Validate data
        $data = $request->only(
            'video_id','scheduled_at','title', 'description');

        $validator = Validator::make($data, [
            'title'         => 'required|string',
            'video_id'      => 'required|exists:videos,id',
            'scheduled_at'  => 'date',
            'description'   => 'required|string'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is valid, update restream
        $restream->update([
            'title'         => $request->title,
            'video_id'      => $request->video_id,
            'description'   => $request->description,
            'scheduled_at'  => $request->scheduled_at,
        ]);

        //Video updated, return success response
        return response()->json([
            'success' => true,
            'message' => 'Restream updated successfully',
            'data' => $restream
        ], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Restream $restream
     * @return JsonResponse
     */
    public function destroy(Restream $restream)
    {
        $restream->delete();

        return response()->json([
            'success' => true,
            'message' => 'restream deleted successfully'
        ], Response::HTTP_OK);
    }
}
