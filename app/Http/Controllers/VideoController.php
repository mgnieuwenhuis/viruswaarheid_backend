<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Models\Video;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class VideoController extends Controller
{
    /**
     * List all processed videos
     */
    public function list(Request $request)
    {
        $validator = Validator::make($request->only('filter'), [
            'sort' => 'array',
            'filter' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $videos = Video::filter($request->filter)->sort($request->sort)->with('publishings', 'rubric', 'tags')->paginate(25);

        return view('videos.index', compact("videos"));
    }

    /**
     * List all processed videos
     */
    public function unpublished(Request $request)
    {
        $validator = Validator::make($request->only('filter'), [
            'sort' => 'array',
            'filter' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $videos = Video::has('publishings')->filter($request->filter)->sort($request->sort)->with('publishings', 'rubric', 'tags')->paginate(25);


        return view('videos.index-never-published', compact("videos"));
    }

    public function uploadVideo(Video $video)
    {
        return view('videos.upload-video', compact("video"));
    }

    public function doUploadVideo(Request $request, Video $video)
    {
        //Validate data
        $data = $request->only('video');

        $validator = Validator::make($data, [
            'video' => 'required|file|mimetypes:video/x-ms-asf,video/x-flv,video/mp4,application/x-mpegURL,video/MP2T,video/3gpp,video/quicktime,video/x-msvideo,video/x-ms-wmv,video/avi',
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $fileName = $request->file('video')->getClientOriginalName() . '_' . time() . '.' . $request->file('video')->extension();
        $fileSize = $request->file('video')->getSize();
        $fileType = $request->file('video')->getClientMimeType();

        $request->video->move(storage_path('videos'), $fileName);

        $video->update([
            'file_name' => $fileName,
            'file_type' => $fileType,
            'file_size' => $fileSize,
        ]);

        //Video updated, return success response
        return response()->json([
            'success' => true,
            'message' => 'Video updated successfully',
        ], Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        //Validate data
        $data = $request->only(
            'title', 'publish_at', 'summary', 'rubric_id',
            'url_media_videowaarheid', 'thumbnail','video',
            'url_article_viruswaarheid', 'tags', 'attachment');

        $validator = Validator::make($data, [
            'title' => 'required|string',
            'publish_at' => 'date',
            'summary' => 'required|string',
            'rubric_id' => 'required|exists:rubrics,id',
            'thumbnail' => 'file|mimes:jpeg,jpg',
            'url_media_videowaarheid' => 'url',
            'url_article_viruswaarheid' => 'url',
            'video' => 'file|mimetypes:video/x-ms-asf,video/x-flv,video/mp4,application/x-mpegURL,video/MP2T,video/3gpp,video/quicktime,video/x-msvideo,video/x-ms-wmv,video/avi',
            'attachment' => 'file',
            'tags' => 'required|string',
        ]);


        //Send failed response if request is not valid
        if ($validator->fails()) {
            return redirect()->back()->withErrors(['message' => $validator->messages()->first()]);
        }

        if($request->attachment) {
            $attachmentName = $request->attachment->getClientOriginalName() . '_' . time() . '.' . $request->attachment->extension();
            $attachmentType = $request->attachment->getClientMimeType();
            $attachmentSize = $request->attachment->getSize();

            $request->attachment->move(storage_path('attachments'), $attachmentName);
        }


        //Request is valid, create new video
        $video = Video::create([
            'title' => $request->title,
            'publish_at' => $request->publish_at,
            'summary' => $request->summary,
            'attachment_name' => $attachmentName ?? null,
            'attachment_type' => $attachmentType ?? null,
            'attachment_size' => $attachmentSize ?? null,
            'rubric_id' => $request->rubric_id,
            'url_media_videowaarheid' => $request->url_media_videowaarheid,
            'url_article_viruswaarheid' => $request->url_article_viruswaarheid,
        ]);



        $toBeSynced = [];
        foreach (explode(',', $request->tags) as $tag)
        {
            if(Tag::find($tag) === null) {
                $toBeSynced[] = (Tag::create([
                    'title' => $tag
                ]))->id;
            } else {
                $toBeSynced[] = $tag;
            }
        }

        $video->tags()->sync($toBeSynced);

        //Video created, return success response
        return redirect()->route('videos.upload-video', ['video' => $video->id]); ;
    }

    public function downloadVideo(Video $video)
    {
        return response()->download(storage_path('videos') . '/' . $video->file_name);
    }

    public function downloadAttachment(Video $video)
    {
        return response()->download(storage_path('attachments') . '/' . $video->attachment_name);
    }

    /**
     * Display the specified resource.
     *
     * @param Video $video
     */
    public function show(Video $video)
    {
        if (!$video) {
            return redirect()->route('dashboard');
        }

        $video->loadMissing(['rubric', 'publishings', 'tags', 'restreams']);

        return view('videos.show', compact("video"));
    }

    public function edit(Request $request, Video $video)
    {
        return view('videos.edit', compact('video'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Video $video
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Video $video)
    {
        //Validate data
        $data = $request->only(
            'title', 'publish_at', 'summary', 'rubric_id',
            'file', 'thumbnail', 'url_media_videowaarheid', 'thumbnail',
            'url_article_viruswaarheid', 'tags', 'attachment');

        $validator = Validator::make($data, [
            'title' => 'required|string',
            'thumbnail' => 'file|mimes:jpeg,jpg',
            'publish_at' => 'date',
            'tags' => 'required|string',
            'rubric_id' => 'required|exists:rubrics,id',
            'summary' => 'required|string',
            'url_media_videowaarheid' => 'url',
            'url_article_viruswaarheid' => 'url',
            'attachment' => 'file'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors(['message' => $validator->messages()->first()]);
        }

        if ($request->thumbnail) {
            $thumbnailName = $request->thumbnail->getClientOriginalName() . '_' . time() . '.' . $request->thumbnail->extension();
            $thumbnailType = $request->thumbnail->getClientMimeType();
            $thumbnailSize = $request->thumbnail->getSize();

            $request->thumbnail->move(storage_path('thumbnails'), $thumbnailName);

            unlink(storage_path('thumbnails') . '/' . $video->thumbnail_name);

            $video->update([
                'thumbnail_name' => $thumbnailName,
                'thumbnail_type' => $thumbnailType,
                'thumbnail_size' => $thumbnailSize,
            ]);
        }

        if($request->attachment) {
            $attachmentName = $request->attachment->getClientOriginalName() . '_' . time() . '.' . $request->attachment->extension();
            $attachmentType = $request->attachment->getClientMimeType();
            $attachmentSize = $request->attachment->getSize();

            $request->attachment->move(storage_path('attachments'), $attachmentName);
        }

        //Request is valid, update video
        $video->update([
            'title' => $request->title,
            'publish_at' => $request->publish_at,
            'summary' => $request->summary,
            'attachment_name' => $attachmentName ?? $video->attachment_name,
            'attachment_type' => $attachmentType ?? $video->attachment_type,
            'attachment_size' => $attachmentSize ?? $video->attachment_size,
            'rubric_id' => $request->rubric_id,
            'url_media_videowaarheid' => $request->url_media_videowaarheid,
            'url_article_viruswaarheid' => $request->url_article_viruswaarheid,
        ]);

        $toBeSynced = [];
        foreach (explode(',', $request->tags) as $tag)
        {
            if(Tag::find($tag) === null) {
                $toBeSynced[] = (Tag::create([
                    'title' => $tag
                ]))->id;
            } else {
                $toBeSynced[] = $tag;
            }
        }

        $video->tags()->sync($toBeSynced);

        return redirect()->route('videos.show', ['video' => $video->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Video $video
     */
    public function destroy(Video $video)
    {
        if ($video->file_name) {
            unlink(storage_path('videos') . '/' . $video->file_name);
        }

        if ($video->thumbnail_name) {
            unlink(storage_path('thumbnails') . '/' . $video->thumbnail_name);
        }

        if ($video->attachment_name) {
            unlink(storage_path('atttachments') . '/' . $video->attachment_name);
        }

        $video->delete();

        return redirect()->route('videos.index');
    }
}
