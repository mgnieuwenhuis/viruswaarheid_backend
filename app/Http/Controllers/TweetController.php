<?php

namespace App\Http\Controllers;

use App\Models\Tweet;
use App\Models\Tweeter;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Validator;

class TweetController extends Controller
{

    /**
     * List all tweets of a tweeter
     * @param Request $request
     * @return JsonResponse
     */
    public function list(Request $request, Tweeter $tweeter)
    {
        $validator = Validator::make($request->only('filter'), [
            'filter' => 'array',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        return Tweet::where('tweeter_id', '=', $tweeter->id)
            ->orderBy('created_at', 'DESC')
            ->filter($request->filter)->paginate(25);
    }

    /**
     * Display the specified resource.
     *
     * @param Tweeter $tweeter
     * @param Tweet $tweet
     * @return Tweet
     */
    public function show(Tweeter $tweeter, Tweet $tweet)
    {
        if (!$tweet) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, tweet not found.'
            ], 400);
        }

        return $tweet;
    }
}
