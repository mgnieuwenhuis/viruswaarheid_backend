<?php

namespace App\Traits;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Builder;

trait Filter
{
    public function scopeFilter(Builder $query, $filters = null)
    {
        if (!empty($filters)) {
            $query->where(function (Builder $query) use ($filters) {
                foreach ($filters as $column => $pattern) {
                    if (Schema::hasColumn($this->getTable(), $column)){
                        $query->where(function (Builder $query) use ($column, $pattern) {
                            $query->where($column, 'LIKE', $pattern)
                                ->orWhere($column, 'LIKE', '%' . $pattern)
                                ->orWhere($column, 'LIKE', $pattern . '%')
                                ->orWhere($column, 'LIKE', '%' . $pattern . '%');
                        });
                    }
                }
            });
        }
    }
}
