<?php

namespace App\Traits;

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Builder;

trait Sort
{
    public function scopeSort(Builder $query, array $sort = null)
    {
        if (!empty($sort)) {
            foreach ($sort as $column => $pattern) {
                if (Schema::hasColumn($this->getTable(), $column)) {
                    $query->orderBy($column, $pattern);
                }
            }
        }
    }
}

