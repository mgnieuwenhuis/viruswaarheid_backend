<?php

namespace App\Listeners;

use Facebook\Facebook;
use App\Events\PublishVideo;
use Illuminate\Support\Facades\Cache;

class CreateFacebookPagePost
{
    private $facebook;

    /**
     * Create the event listener.
     *
     * @return void
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function __construct()
    {
        $this->facebook = new Facebook([
            'app_id' => config('facebook.app_id'),
            'app_secret' => config('facebook.app_secret'),
            'default_graph_version' => 'v2.2',
        ]);
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\PublishVideo  $event
     * @return void
     */
    public function handle(PublishVideo $event)
    {
        //Post property to Facebook
        $linkData = [
            'link' => $event->video->url_article_viruswaarheid,
            'message' => $event->video->title . "\r\n" . $event->video->summary
        ];

        try {
            $response = $this->facebook->post('/me/feed', $linkData, Cache::get('facebook.page_token'));
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: '.$e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: '.$e->getMessage();
            exit;
        }
        $graphNode = $response->getGraphNode();
    }
}
