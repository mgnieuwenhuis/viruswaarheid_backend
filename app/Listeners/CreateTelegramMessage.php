<?php

namespace App\Listeners;

use App\Events\PublishVideo;

class CreateTelegramMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param \App\Events\PublishVideo $event
     * @return void
     */
    public function handle(PublishVideo $event)
    {
        $botToken = config('telegram.token');
        $chat_id = config('telegram.chat_id');
        $message = $event->video->url_article_viruswaarheid
            . "\r\n"
            . $event->video->title
            . "\r\n"
            . $event->video->summary;
        $bot_url = "https://api.telegram.org/bot$botToken/";
        $url = $bot_url . "sendMessage?chat_id=" . $chat_id . "&text=" . urlencode($message);
        file_get_contents($url);
    }
}
