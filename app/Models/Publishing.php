<?php

namespace App\Models;

use App\Traits\Sort;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Publishing extends Model
{
    use Sort;
    use Uuids;
    use HasFactory;

    public function video(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Video::class);
    }
}
