<?php

namespace App\Models;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    use Uuids;
    use HasFactory;

    public const ADMIN = 1;
    public const EDITOR = 2;
    public const USER = 3;

    protected $fillable = [
        'role_id',
    ];


    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
