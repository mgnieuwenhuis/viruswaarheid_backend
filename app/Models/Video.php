<?php

namespace App\Models;

use App\Traits\Sort;
use App\Traits\Uuids;
use App\Traits\Filter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Video extends Model
{
    use Sort;
    use Uuids;
    use Filter;
    use HasFactory;

    protected $fillable = [
        'title', 'summary',
        'url_media_videowaarheid', 'url_article_viruswaarheid',
        'file_name', 'file_size','file_type',
        'thumbnail_name', 'thumbnail_type', 'thumbnail_size',
        'attachment_name', 'attachment_type', 'attachment_size',
        'rubric_id'
    ];

    public function tags(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Tag::class);
    }

    public function publishings(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Publishing::class);
    }

    public function restreams(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Restream::class);
    }

    public function rubric(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Rubric::class);
    }
}
