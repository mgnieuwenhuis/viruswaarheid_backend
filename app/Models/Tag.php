<?php

namespace App\Models;

use App\Traits\Sort;
use App\Traits\Uuids;
use App\Traits\Filter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Tag extends Model
{
    use Sort;
    use Uuids;
    use Filter;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
    ];

    public function videos(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Video::class);
    }

    public function getValueAttribute($value)
    {
        return $value . " " . $this->videos()->count() . "x";
    }
}
