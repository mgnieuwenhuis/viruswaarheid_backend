<?php
namespace App\Models;

use App\Traits\Sort;
use App\Traits\Uuids;
use App\Traits\Filter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable
{
    use Sort;
    use HasFactory, Notifiable;
    use Uuids;
    use Filter;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(UserRole::class);
    }

    public function addRole(int $roleId)
    {
        if(!$this->hasRole($roleId)) {
            $this->roles()->create(['role_id' => $roleId]);
        }
    }

    public function removeRole(int $roleId)
    {
        if ($role = $this->roles()->where(['role_id' => $roleId])->first()) {
            $role->delete();
        }
    }

    public function hasRole(int $roleId)
    {
        return $this->whereRelation('roles', 'role_id', $roleId)->exists();
    }

    public function isAdmin(): bool
    {
        return $this->hasRole(UserRole::ADMIN);
    }

    public function isEditor(): bool
    {
        return $this->hasRole(UserRole::EDITOR);
    }

    public function isUser(): bool
    {
        return $this->hasRole(UserRole::USER);
    }
}
