<?php

namespace App\Models;

use App\Traits\Filter;
use App\Traits\Sort;
use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tweeter extends Model
{
    use Sort;
    use Uuids;
    use Filter;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'twitter_id',
    ];

    public function tweets(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Tweet::class)->orderBy('tweets.tweet_created_at', 'DESC');
    }
}
