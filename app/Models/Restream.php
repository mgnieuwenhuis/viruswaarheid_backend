<?php

namespace App\Models;

use App\Traits\Sort;
use App\Traits\Uuids;
use App\Traits\Filter;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restream extends Model
{
    use Sort;
    use Uuids;
    use Filter;
    use HasFactory;

    public const WEBSITE = 0;
    public const FACEBOOK = 1;
    public const TELEGRAM = 2;

    protected $fillable = [
        'title', 'scheduled_at', 'description', 'video_id', 'media'
    ];

    public function video(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Video::class);
    }

}
