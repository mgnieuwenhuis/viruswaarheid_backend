<?php

namespace App\Console\Commands;

use App\Models\Video;
use Illuminate\Console\Command;
use \Dcblogdev\Dropbox\Facades\Dropbox;
use Illuminate\Support\Facades\Storage;

class DropboxRunner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vw:dropbox-runner';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download new dropbox files from the /videos folder, saves it locally
    and insert the metadata to the database.
    ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */

    function get_mime_type($filename) {
        $idx = explode( '.', $filename );
        $count_explode = count($idx);
        $idx = strtolower($idx[$count_explode-1]);

        $mimet = [
            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',
            'mp4' => 'video/mp4',
            'mpeg' => 'video/mpeg',
            'avi' => 'video/x-msvideo',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',
            'docx' => 'application/msword',
            'xlsx' => 'application/vnd.ms-excel',
            'pptx' => 'application/vnd.ms-powerpoint',


            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        ];

        if (isset( $mimet[$idx] )) {
            return $mimet[$idx];
        } else {
            return 'application/octet-stream';
        }
    }

    public function handle()
    {
        $videos = Dropbox::post('files/list_folder/',
            [ "path" => "/videos" ]);

        foreach ($videos["entries"] as $video) {
            $metaData = Dropbox::post('files/get_metadata', [
                "path" => $video["path_display"],
            ]);

            if ($metaData["is_downloadable"] === true) {
                if (Video::where('file_size', $metaData["size"])
                    ->count() === 0) {
                    $videoName      = $metaData["name"];
                    $thumbnailName  = substr($videoName, 0, strrpos($videoName, '.')) . ".jpg";
                    $pathVideos     = storage_path('videos');
                    $pathThumbnails = storage_path('thumbnails');

                    Dropbox::files()->download($video["path_display"], $pathVideos . '/');

                    exec("/usr/bin/ffmpegthumbnailer -i $pathVideos/$videoName -o $pathThumbnails/$thumbnailName -s 0 -q 10");

                    Video::create([
                        'title' => preg_replace("/\.[^.]+$/", "", $metaData["name"]),
                        'file_name' => $videoName,
                        'file_size' => $metaData["size"],
                        'file_type' => $this->get_mime_type($videoName),
                        'thumbnail_name' => $thumbnailName,
                        'thumbnail_size' => filesize($pathThumbnails . "/" . $thumbnailName),
                        'thumbnail_type' => $this->get_mime_type($thumbnailName),
                    ]);
                }
            }
        }
    }
}
