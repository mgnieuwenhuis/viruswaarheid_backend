<?php

namespace App\Console\Commands;

use App\Events\PublishVideo;
use App\Models\Video;
use Illuminate\Console\Command;

class SocialMediaPusher extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vw:push-to-sm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'First post video to WP and then create event to
    push video to social media (Telegram, Facebook)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function createSlug($str, $delimiter = '-')
    {
        $slug = strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));

        return $slug;
    }

    public function wpGetPostXMLRPC($postId)
    {
        $params = [
            $postId,
            config('wordpress.username'),
            config('wordpress.password'),
        ];

        $request = xmlrpc_encode_request('metaWeblog.getPost', $params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_URL, config('wordpress.rpc_url'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        $results = curl_exec($ch);
        curl_close($ch);

        $xml = simplexml_load_string($results, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);

        foreach ($array["params"]["param"]["value"]["struct"]["member"] as $member) {
            if($member["name"] === "permaLink") {
                return $member["value"]["string"];
            }
        }

        return false;
    }

    public function wpUploadImageXMLRPC(Video $video)
    {
        $rpcurl     = config('wordpress.rpc_url');
        $username   = config('wordpress.username');
        $password   = config('wordpress.password');

        $blogid=1; //Post ID

        $file     = file_get_contents(storage_path('thumbnails_text') . "/" . $video->thumbnail_name);
        $filetype = "image/jpeg";
        $filename = "remote_filename.jpg";

        xmlrpc_set_type($file,'base64'); // <-- required!
        $params = array($blogid,$username,$password,
            array(
                'name'=>$filename,
                'type'=>$filetype,
                'bits'=>$file,
                'overwrite'=>false
            )
        );
        $request = xmlrpc_encode_request('wp.uploadFile',$params);

        $result = $this->go($request,$rpcurl);
        $xml = simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);

        return (int)$array["params"]["param"]["value"]["struct"]["member"][0]["value"]["string"];
    }

    private function go($request,$rpcurl){
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

            curl_setopt($ch,CURLOPT_POST,1);
            curl_setopt($ch,CURLOPT_URL,$rpcurl);
            curl_setopt($ch,CURLOPT_POSTFIELDS,$request );
            curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
            $result = curl_exec($ch);

            return $result;
        }
    public function wpCreatePostXMLRPC(Video $video, $imageId)
    {
        $encoding = 'UTF-8';
        $keywords = implode(',', $video->tags->pluck('title')->all());

        $content = [
            'title' => $video->title,
            'description' => htmlentities($video->summary, ENT_NOQUOTES, $encoding),
            'wp_post_thumbnail' => $imageId,
            'mt_allow_comments' => 0,
            'mt_allow_pings' => 0,
            'post_type' => 'post',
            'mt_keywords' => $keywords,
            'categories' => array(
                config('wordpress.category')
            ),
            'wp_slug' => $this->createSlug($video->title)
        ];

        $params = array(
            0,
            config('wordpress.username'),
            config('wordpress.password'),
            $content,
            true
        );

        $request = xmlrpc_encode_request('metaWeblog.newPost', $params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_URL, config('wordpress.rpc_url'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);

        $results = curl_exec($ch);

        curl_close($ch);

        $xml = simplexml_load_string($results, "SimpleXMLElement", LIBXML_NOCDATA);
        $json = json_encode($xml);
        $array = json_decode($json, TRUE);

        return $array["params"]["param"]["value"]["string"];
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $videos = Video::whereDate('publish_at', '<=', date('Y-m-d'))->get();

        foreach ($videos as $video) {
            $this->call('vw:create-thumbnail-text-image', [
                'videoId' => $video->id
            ]);

            $imageId  = $this->wpUploadImageXMLRPC($video);
            $postID   = $this->wpCreatePostXMLRPC($video, $imageId);
            $postLink = $this->wpGetPostXMLRPC($postID);

            $video->url_article_viruswaarheid = $postLink;

            PublishVideo::dispatch($video);

            $video->publish_at = null;

            $video->save();
        }
    }
}
