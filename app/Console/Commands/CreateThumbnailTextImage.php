<?php

namespace App\Console\Commands;

use App\Models\Video;
use Illuminate\Console\Command;

class CreateThumbnailTextImage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vw:create-thumbnail-text-image {videoId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a thumbnail text image for a video';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $video = Video::find($this->argument('videoId'));

        if ($video !== null) {
            exec("/usr/bin/wkhtmltoimage "
                . route('thumbnails.html', ['video' => $video->id])
                . " "
                . storage_path('thumbnails_text') . "/" . $video->thumbnail_name . " 2>&1"
            );
        }
    }
}
