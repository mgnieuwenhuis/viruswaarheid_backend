<?php

namespace App\Console\Commands;

use App\Models\Tweet;
use App\Models\Tweeter;
use Illuminate\Console\Command;

class RetrieveTweets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vw:retrieve-tweets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Retrieve the latest tweets of the saved monitored tweeters';

    private $tweets = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    function buildBaseString($baseURI, $method, $params)
    {
        $r = [];

        ksort($params);

        foreach ($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }

        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    function buildAuthorizationHeader($oauth)
    {
        $r = 'Authorization: OAuth ';
        $values = [];

        foreach ($oauth as $key => $value)
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        $r .= implode(', ', $values);

        return $r;
    }

    public function retrieveTweetsOfUserId($userId, $paginationToken = null)
    {
        $url = "https://api.twitter.com/2/users/$userId/tweets";

        $oauth_access_token = config('twitter.access_token');
        $oauth_access_token_secret = config('twitter.access_token_secret');
        $consumer_key = config('twitter.app_key');
        $consumer_secret = config('twitter.app_secret');

        $oauth = [
            'oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $oauth_access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0',
            'max_results' => '100',
            'tweet.fields' => 'created_at',
        ];

        if ($paginationToken !== null) {
            $oauth['pagination_token'] = $paginationToken;
        }

        $base_info = $this->buildBaseString($url, 'GET', $oauth);

        $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;


        // Make requests
        $header = [$this->buildAuthorizationHeader($oauth), 'Expect:'];
        $options = [
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url . '?max_results=100&tweet.fields=created_at' . ($paginationToken !== null ? '&pagination_token=' . $paginationToken : ''),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false
        ];

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);

        $tweetData = (array)json_decode($json);

        if ($tweetData !== null) {

            $finished = false;
            foreach ($tweetData["data"] as $tweet) {
                $tweetArray = (array)$tweet;
                if (Tweet::where('twitter_id', '=', $tweetArray["id"])->doesntExist()) {
                    $this->tweets[] = $tweetArray;
                } else {
                    //abort, we don't need to go further
                    $finished = true;
                    break;
                }
            }

            if (isset($tweetData["meta"]->next_token) && $finished === false) {
                $this->retrieveTweetsOfUserId($userId, $tweetData["meta"]->next_token);
            }
        }
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tweeters = Tweeter::all();

        foreach ($tweeters as $tweeter) {
            $this->retrieveTweetsOfUserId($tweeter->twitter_id);

            foreach ($this->tweets as $tweet) {
                $tweeter->tweets()->create([
                    'text' => $tweet["text"],
                    'twitter_id' => $tweet["id"],
                    'tweet_created_at' => date('Y-m-d H:i:s', strtotime($tweet["created_at"])),
                ]);
            }

            $this->tweets = [];
        }
    }
}
