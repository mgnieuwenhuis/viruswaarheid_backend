<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class RenewFacebookToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vw:renew-fb-token';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update the Facebook Page Access Token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $token_url = "https://graph.facebook.com/oauth/access_token?client_id="
            . config('facebook.app_id')
            . "&client_secret=" . config('facebook.app_secret')
            . "&grant_type=fb_exchange_token&fb_exchange_token="
            . Cache::get('facebook.page_token', config('facebook.page_token'));

        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_URL, $token_url);
        $contents = curl_exec($c);
        curl_close($c);

        Cache::put('facebook.page_token', json_decode($contents)->access_token );
    }
}
